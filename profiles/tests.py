from django.test import TestCase
from django.contrib.auth import get_user_model 
from .models import Profile 

User = get_user_model()


class ProfileTestCase(TestCase):

    def setUp(self):
        self.user_a = User.objects.create_user(username = 'abc', password = 'abcd1234')
        self.user_b = User.objects.create_user(username = 'abc-2', password = 'abcd1234')

    def test_profile_created_via_signal(self):
        qs = Profile.objects.all()
        self.assertEqual(qs.count(), 2)

    def test_following(self):
        first       = self.user_a
        second      = self.user_b
        first_profile = first.profile
        first_profile.followers.add(second)
        qs = second.following.filter(user = first)
        self.assertTrue(qs.exists())
        first_user_followers = first.following.all() 
        self.assertFalse(first_user_followers.exists())


     

