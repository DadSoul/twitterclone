from rest_framework import serializers
from .models import Profile


class ProfileSerializer(serializers.ModelSerializer):
    
    first_name      = serializers.SerializerMethodField(read_only = True)
    last_name       = serializers.SerializerMethodField(read_only = True)
    username        = serializers.SerializerMethodField(read_only = True)
    follower_count  = serializers.SerializerMethodField(read_only = True)
    following_count = serializers.SerializerMethodField(read_only = True)
    
    class Meta: 
         model  = Profile 
         fields = [
             'first_name',
             'last_name',
             'id',
             'follower_count',
             'following_count',
             'bio',
             'location',
             'username'
        ]

    # for custom fields 
    # 
    def get_username(self, obj):
        return obj.user.username   

    def get_first_name(self, obj):
        return obj.user.first_name 
  
    def get_last_name(self, obj):
        return obj.user.last_name

    def get_following_count(self, obj): 
        return obj.user.following.count()

    def get_follower_count(self, obj):
        return obj.followers.count()
