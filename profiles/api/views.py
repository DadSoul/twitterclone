from django.http                    import HttpResponse, Http404, JsonResponse
from rest_framework.response        import Response
from rest_framework.decorators      import api_view, permission_classes, authentication_classes
from rest_framework.permissions     import IsAuthenticated
from ..models                       import Profile
from django.utils.http              import is_safe_url
from django.conf                    import settings
from django.contrib.auth            import get_user_model

User = get_user_model()

ALLOWED_HOSTS   = settings.ALLOWED_HOSTS

@api_view(["GET",'POST']) 
@permission_classes([IsAuthenticated])
def user_follow_view(request, username, *args, **kwargs):
    current_user                          = request.user 
    current_username                      = current_user.username

    # checking if the same user follows himself 

    to_follow_user                        = User.objects.filter(username = username)
    Profile.objects.filter(user__username = username).first()
    
    if to_follow_user.exists() == False:
        return Response({}, status = 404)
 
    other       = to_follow_user.first() 
    profile     = other.profile
    data        = request.data or {}
    action      = data.get("action")
    
    if action == "follow": 
        profile.followers.add(current_user) 
    elif action == "unfollow":
        profile.followers.remove(current_user)
    else: 
        pass
    
    current_followers = profile.followers.all()
    
    return Response(
        {
            "followers_count": current_followers.count()
        }, status = 200)
 









 