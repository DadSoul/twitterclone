from rest_framework import serializers 
from django.contrib.auth.models import User 
from django.contrib.auth import authenticate
from knox.models import AuthToken
import json


class UserSerializer(serializers.ModelSerializer):
    class Meta: 
        model   = User 
        fields  = ("id", "username","email")

# Serializer for registration 
class UserRegisterSerializer(serializers.ModelSerializer):
    password2           = serializers.CharField(style = {'input_type':'password2'}, write_only = True)
    token               = serializers.SerializerMethodField(read_only = True)
    message             = serializers.SerializerMethodField(read_only = True)
    
    class Meta: 
        model = User
        fields = [
            'username',
            'email',
            'password', 
            'password2',
            'token',
            'message'
        ]
        extra_kwargs = {'password': {'write_only':True}}

    def get_message(self, obj):
        return "Thank you for registering, Please verify your email"

    def validate_email(self, value):
        qs = User.objects.filter(email__iexact = value)
        if qs.exists(): 
            raise serializers.ValidationError("User with this email already exists")
        return value

    def get_token(self, obj):  
        user = obj
        token = AuthToken.objects.create(user)[1]
        return token   
   
    def validate_username(self, value):
        qs = User.objects.filter(username__iexact = value)
        if qs.exists(): 
            raise serializers.ValidationError("User with this username already exists")
        return value

    def validate(self, data): 
        pw = data.get('password')
        pw2 = data.pop('password2')
        if pw != pw2: 
            raise serializers.ValidationError("Passwords do not match")
        return data

    def create(self, validated_data):
        user_obj = User.objects.create(
            username = validated_data.get('username'),
            email    = validated_data.get('email'))
        user_obj.set_password(validated_data.get('password'))
        user_obj.is_active = False
        user_obj.save() 
        return user_obj