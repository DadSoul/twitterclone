from rest_framework import generics, permissions
from rest_framework.response import Response
from knox.models import AuthToken
from .serializers import UserSerializer,UserRegisterSerializer
from django.contrib.auth.models import User
from rest_framework.views import APIView
from django.db.models import Q
from django.contrib.auth import authenticate
from django.contrib.auth import login, logout
from knox.views import LogoutView

class RegisterAPIView(generics.CreateAPIView):
    queryset            = User.objects.all()
    serializer_class    = UserRegisterSerializer 

    def get_serializer_context(self, *args, **kwargs):
        return {"request":self.request}
