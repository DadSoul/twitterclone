from django.shortcuts   import render
from django.http        import HttpResponse, Http404, JsonResponse
from django.shortcuts   import render, redirect
from rest_framework.response    import Response

import random
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication
from .models            import Tweet
from .forms             import TweetForm
from django.utils.http  import is_safe_url
from django.conf        import settings
from .serializers       import (
    TweetSerializer, 
    TweetActionSerializer, 
    TweetCreateSerializer
)

ALLOWED_HOSTS   = settings.ALLOWED_HOSTS

def home_view(request, *args, **kwargs):
    return render(request, "pages/feed.html")

def tweets_list_view(request, *args, **kwargs):
    return render(request, "tweets/list.html")

def tweets_detail_view(request, tweet_id, *args, **kwargs):
    return render(request, "tweets/detail.html", context = {"tweet_id":tweet_id})
