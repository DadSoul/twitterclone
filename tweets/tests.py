from django.test            import TestCase
from .models                import Tweet
from django.contrib.auth    import get_user_model
from rest_framework.test    import APIClient

User = get_user_model()

class TweetTestCase(TestCase):
    '''
        Testing views in urls of tweets
    '''
    
    def setUp(self):
        self.user = User.objects.create_user(username = 'abc', password = 'abcd1234')
        for _ in range(3):
            Tweet.objects.create(content = 'my tweet', user = self.user)
        self.currentCount   = Tweet.objects.all().count()

    def test_user_exists(self):
        self.assertEqual(self.user.username, 'abc')

    def test_tweet_created(self):
        tweet = Tweet.objects.create(content = 'my tweet', user = self.user)
        self.assertEqual(tweet.id, 4)
        self.assertEqual(tweet.user, self.user)
    
    def get_client(self, is_random = False):
        client = APIClient()
        if not is_random: 
            client.login(username = self.user.username, password = 'abcd1234')
        return client

    def test_tweet_list(self):
        client      = self.get_client()
        response    = client.get('/api/tweets/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 3)

    def test_action_like(self):
        client      = self.get_client()
        response    = client.post('/api/tweets/action/', {'id':1, 'action':'like'})
        self.assertEqual(response.status_code, 200)
        like_count  = response.json().get('likes')
        self.assertEqual(like_count, 1)
        user = self.user
        my_like_instances_count = user.tweetlike_set.count()
        self.assertEqual(my_like_instances_count, 1)
        my_related_likes = user.tweet_user.count()
        self.assertEqual(my_related_likes, 1)

    def test_action_unlike(self):
        client      = self.get_client()
        response    = client.post('/api/tweets/action/', {'id':2, 'action':'like'})
        self.assertEqual(response.status_code, 200)
        response    = client.post('/api/tweets/action/', {'id':2, 'action':'unlike'})
        self.assertEqual(response.status_code, 200)
        like_count  = response.json().get('likes')
        self.assertEqual(like_count, 0)
    
    def test_action_retweet(self):
        client          = self.get_client()
        currentCount    = self.currentCount
        response    = client.post('/api/tweets/action/', {'id':2, 'action':'retweet'})
        self.assertEqual(response.status_code, 201)
        data            = response.json()
        new_tweet_id    = data.get("id")
        self.assertNotEqual(2, new_tweet_id)
        self.assertEqual(currentCount + 1, new_tweet_id)

    def test_tweet_create_api_view(self):
        data = {'content':'test tweet'}
        client          = self.get_client()
        response        = client.post('/api/tweets/create/', data)
        self.assertEqual(response.status_code, 201)
        response_data   = response.json()
        new_tweet_id    = response_data.get('id')
        self.assertEqual(self.currentCount + 1, new_tweet_id)

    def test_detail_view(self):
        random_client           = self.get_client(is_random=True)
        tweet_id                = 1
        response                = random_client.get('/api/tweets/' + str(tweet_id) + '/')
        self.assertEqual(response.status_code, 200)
        response_data           = response.json()
        requested_id            = response_data.get('id')
        self.assertEqual(requested_id, 1)
        tweet_id                = 10
        response                = random_client.get('/api/tweets/' + str(tweet_id) + '/')
        self.assertEqual(response.status_code, 404) 

    def test_delete_view(self):
        # checking for random client (not logged in)
        random_client           = self.get_client(is_random=True)
        tweet_id                = 1
        response                = random_client.delete('/api/tweets/delete/' + str(tweet_id) + '/') 
        self.assertEqual(response.status_code, 403)
        # checking for logged client
        logged_client           = self.get_client(is_random=False)
        response                = logged_client.delete('/api/tweets/delete/' + str(tweet_id) + '/') 
        self.assertEqual(response.status_code, 200)
