from rest_framework import serializers
from .models import Tweet
from django.conf import settings
from profiles.serializers import ProfileSerializer

MAX_TWEET_LENGTH        = settings.MAX_TWEET_LENGTH
TWEET_ACTION_OPTIONS    = settings.TWEET_ACTION_OPTIONS  

class TweetActionSerializer(serializers.Serializer):
    id      = serializers.IntegerField()
    action  = serializers.CharField()
    content = serializers.CharField(allow_blank = True, required = False)

    def validate_action(self, value):
        value = value.lower().strip()
        if not value in TWEET_ACTION_OPTIONS:
            raise serializers.ValidationError("This is not allowed aciton for tweets")
        return value

class TweetCreateSerializer(serializers.ModelSerializer):
    likes       = serializers.SerializerMethodField(read_only = True)
    user        = ProfileSerializer(source = 'user.profile', read_only = True)

    class Meta: 
        model = Tweet
        fields = ['content', 'likes', 'id', 'timestamp','user']

    def get_likes(self, obj):
        return 0

    def validate_content(self, value):
        if len(value) > MAX_TWEET_LENGTH: 
            raise serializers.ValidationError("This tweet is too long")
        return value

class TweetSerializer(serializers.ModelSerializer):
    likes       = serializers.SerializerMethodField(read_only = True)
    content     = serializers.SerializerMethodField(read_only = True)
    parent      = TweetCreateSerializer(read_only = True)
    user        = ProfileSerializer(source = 'user.profile', read_only = True)

    class Meta: 
        model = Tweet
        fields = [
            'content', 
            'likes', 
            'id', 
            'is_retweet', 
            'parent',
            'user',
            'timestamp'
        ]

    def get_content(self, obj): 
        content = obj.content
        if obj.is_retweet:  
            content = obj.parent.content
        return content

    def get_likes(self, obj):
        return obj.likes.count()

