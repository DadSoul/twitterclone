from django.shortcuts   import render
from django.http        import HttpResponse, Http404, JsonResponse
from django.shortcuts   import render, redirect

from rest_framework.response    import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model

User = get_user_model() 

import random

from ..models            import Tweet
from ..forms             import TweetForm
from django.utils.http  import is_safe_url
from django.conf        import settings
from ..serializers       import (
    TweetSerializer, 
    TweetActionSerializer, 
    TweetCreateSerializer
)

ALLOWED_HOSTS   = settings.ALLOWED_HOSTS

def get_paginated_queryset_response(qs, request):
    paginator           = PageNumberPagination() 
    paginator.page_size = 10
    paginated_qs        = paginator.paginate_queryset(qs, request)
    serializer = TweetSerializer(paginated_qs,  many = True)
    return paginator.get_paginated_response(serializer.data)

# by using function views 
@api_view(['POST']) # http method the client == POST 
@permission_classes([IsAuthenticated])
def tweet_create_view(request, *args, **kwargs):
    data        = request.data
    # print(data)
    # user        = request.data["user"] or None
    
    # if user is None: 
    #     return Response({}, status = 403)
    
    # user_obj = User.objects.get(user__username = user)

    # if not user_obj.exists(): 
    #     return Response({}, status = 403)

    # if not user_obj.is_authenticated: 
    #     return Response({}, status = 403)

    serializer  = TweetCreateSerializer(data=data)
    if serializer.is_valid(raise_exception = True): 
        serializer.save(user = request.user)
        return Response(serializer.data, status = 201)
    return Response({}, status = 400)
 
@api_view(['GET'])
def tweet_list_view(request, *args, **kwargs):
    qs = Tweet.objects.all()
    username = request.GET.get('username') #?username=Akezhan
    if username != None: 
        # custom filtering by TweetQuerySet
        qs = qs.by_username(username)
    return get_paginated_queryset_response(qs, request)

@api_view(['GET'])
# @permission_classes([IsAuthenticated])
def tweet_feed_view(request, *args, **kwargs):
    user                = request.user 
    qs                  = Tweet.objects.feed(user) 
    return get_paginated_queryset_response(qs, request)

@api_view(['GET'])
def tweet_detail_view(request, tweet_id, *args, **kwargs):
    qs = Tweet.objects.filter(id = tweet_id)
    if not qs.exists(): 
        return Response({}, status = 404)
    obj = qs.first()
    serializer = TweetSerializer(obj)
    return Response(serializer.data, status = 200)

@api_view(['DELETE', 'POST'])
# @permission_classes([IsAuthenticated])
def tweet_delete_view(request, tweet_id, *args, **kwargs):
    qs       = Tweet.objects.filter(id = tweet_id)
    if not qs.exists(): 
        return Response({}, status = 404)
    qs = qs.filter(user = request.user)
    if not qs.exists(): 
        return Response({'message': 'no way to delete'}, status = 401)
    obj = qs.first()
    obj.delete()
    return Response({'message':'Tweet removed'}, status = 200)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def tweet_action_view(request, *args, **kwargs):
    '''
        id is required. 
        Like, Unlike, retweet
    '''
    serializer = TweetActionSerializer(data = request.data)

    if serializer.is_valid(raise_exception=True):
        data        = serializer.validated_data
        tweet_id    = data.get('id')
        action      = data.get('action')
        content     = data.get('content')
        qs = Tweet.objects.filter(id = tweet_id)
        
        if not qs.exists(): 
            return Response({}, status = 404)
        obj = qs.first()

        if action == 'like':
            obj.likes.add(request.user)
            serializer = TweetSerializer(obj)
            return Response(serializer.data, status = 200)
        elif action == 'unlike':
            obj.likes.remove(request.user)
            serializer = TweetSerializer(obj)
            return Response(serializer.data, status = 200)
        elif action == 'retweet':
            new_tweet   = Tweet.objects.create(
                user = request.user,
                parent = obj, 
                content = content
                )  
            serializer = TweetSerializer(new_tweet)
            return Response(serializer.data, status = 201)
            
    return Response({"message":"Tweet modified"}, status = 200)
 
