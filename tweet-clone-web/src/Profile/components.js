import React from 'react' 

export function UserDetails(props){
    const {user, includeFullName} = props
    const nameDisplay = includeFullName === true ? `${user.first_name} ${user.last_name}` : null
    
    const handleLink = (event) => {
         window.location.href = `/profiles/${user.username}`
    }

    return <div>
                <p>
                    {nameDisplay}{" "}
                    <span onClick = {handleLink}>@{user.username}</span> 
                </p>
            </div>
}

export function UserPicture(props){
    const {user} = props

    return  <span className = 'mx-1 px-3 py-2 rounded-circle bg-dark text-white'>
                {user.username[0]}
            </span>
}