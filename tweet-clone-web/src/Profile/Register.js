import React, {Component} from "react";
import {connect} from 'react-redux';
import {Link, Redirect} from 'react-router-dom';
import {register} from '../actions/auth';


function LabelComponent(props){
    return <div>
            <label className = "ml-4" htmlFor={props.name}>{props.name}</label>
        </div>

}


class Register extends Component {

    constructor(props){
        super(props)
        this.handleErrorComing = this.handleErrorComing.bind(this);
    }
    
    state = {
        username: "",
        password: "",
        password2: "",
        email:"", 
        warnings: ""
    }

    handleErrorComing = (response) => {
        console.log("RESPONSE")
        console.log(response)
        let localWarnings = "";
        for (const [key, value] of Object.entries(response)) {
            localWarnings +=key + ": " + value;
            localWarnings += " ";
        }
        this.setState({warnings:localWarnings})
    }

    onSubmit = (event) =>{
        event.preventDefault();
        const data = {
            username:   this.state.username, 
            password:   this.state.password, 
            password2:  this.state.password2,
            email:      this.state.email
        } 
        console.log("REGISTER SUBMIT")
        this.props.register(data, this.handleErrorComing)
    }


    render(){
        return(
            <form onSubmit={this.onSubmit}>
        <fieldset>
          <legend className= "ml-4">Register</legend>
          {this.state.warnings.length > 0 && (
            <ul>
              {this.state.warnings}
            </ul>
          )}
            
                <LabelComponent name="username"/>
                <input
                className = 'ml-4'
                type="text" id="username"
                onChange={e => this.setState({username: e.target.value})} />
            
            <LabelComponent name="email"/>
            <input
            className = 'ml-4'
            type="text" id="email"
            onChange={e => this.setState({email: e.target.value})} />
            
            <LabelComponent name="password"/>
    
            <input
            className = 'ml-4'
            type="password" id="password"
            onChange={e => this.setState({password: e.target.value})} />
           
            <LabelComponent name="password2"/>
            <input
            className = 'ml-4'
            type="password" id="password2"
            onChange={e => this.setState({password2: e.target.value})} />
            
            <p className = 'ml-4 mt-4'>
                <button type="submit">Register</button>
            </p>
          <p className = 'ml-4'>
            Already have an account? <Link to="/login">Login</Link>
          </p>
        </fieldset>
      </form>
        )
    }
}


const mapStateToProps = state => {
    let errors = [];
    if (state.auth.errors) {
        errors = Object.keys(state.auth.errors).map(field => {
          return {field, message: state.auth.errors[field]};
        });
      }
      return {
        errors,
        isAuthenticated: state.auth.isAuthenticated, 
        data: state.auth.data
      };
}

const mapDispatchToProps = dispatch => {
    return {
        register: (data, callback) => dispatch(register(data, callback))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);
