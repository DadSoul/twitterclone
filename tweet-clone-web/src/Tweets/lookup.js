import {backendLookup} from '../LookUp'

export function apiTweetCreate(newTweet, callback, token){
    console.log("API TWEET CREATE")
    console.log(token)
    backendLookup('POST', '/tweets/create/', callback, {content: newTweet}, token)
}

export function apiTweetDetail(tweetId, callback){
    let endpoint = `/tweets/${tweetId}/`
    backendLookup("GET", endpoint, callback)
}

export function apiTweetList(username, callback, nextUrl, prevUrl){
    let endpoint = '/tweets/'  

    if (username){
        endpoint = `/tweets/?username=${username}`
    }

    const url_base = "http://localhost:8000/api"

    if (nextUrl !== null && nextUrl !== undefined){
        endpoint = nextUrl.replace(url_base, "")
    }

    if (prevUrl !== null && prevUrl !== undefined){
        endpoint = prevUrl.replace(url_base, "")
    }

    backendLookup("GET", endpoint, callback)
}

export function apiTweetFeed(callback){
    let endpoint = '/tweets/feed/'  
    const url_base = "http://localhost:8000/api"
    backendLookup("GET", endpoint, callback)
}

export function apiTweetAction(tweetId, action, callback, token = null){
    const data = {id: tweetId, action: action}
    backendLookup("POST", "/tweets/action/", callback, data, token)
}  