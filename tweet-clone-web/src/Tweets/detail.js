import React, {useState, useEffect}  from 'react'

import {ActionButton} from './buttons'
import { TweetCreate } from '.';
import {UserDetails, UserPicture} from '../Profile'


export function ParentTweet(props){
    const {tweet} = props

    return tweet.parent ? 
        <Tweet isRetweet reTweeter = {props.reTweeter} hideActions className = {' '} tweet = {tweet.parent}/>:null
}

 
export function Tweet(props){
    const {tweet, didRetweet, hideActions, isRetweet, reTweeter}     = props
    const [actionTweet, setActionTweet] = useState(props.tweet ? props.tweet : null)
    let className   = props.className ? props.className : 'col-10 mx-auto col-md-6'
    className       = isRetweet === true ? `${className} p-2 border rounded` : `${className}`
    const path        = window.location.pathname
    const idRegex = /(?<tweetid>\d+)/ 
    const match = path.match(idRegex) 
    const urlTweetId = match ? match.groups.tweetid : -1
    const isDetail    = `${tweet.id}` === `${urlTweetId}`
    const {token}    = props

    const handlePerformAction = (newActionTweet, status) => {
        if (status === 200){
            setActionTweet(newActionTweet)
        }else if (status === 201){
            // let the list know
            if (didRetweet){
                didRetweet(newActionTweet)
            }
        }
    }

    const handleLink = (event) => {
        event.preventDefault()
        window.location.href = `http://localhost:8000/${tweet.id}/`
    }

    return <div className = {className}>
                {isRetweet === true && <div className = 'mb-2'>
                <span className = 'small text-mutex'>Retweet via <UserDetails user = {reTweeter}/></span>
            </div>}
        <div className = 'd-flex'>
        <div className = ''>
            <UserPicture user = {tweet.user}/>
        </div>
        <div className = 'col-11'>
            <div>
                <UserDetails user = {tweet.user} includeFullName />
                <p>{tweet.content}</p>
                    {/* {isRetweet === true && <span className = 'small text-mutex'>Retweet</span>} */}
                <ParentTweet tweet = {tweet} reTweeter = {tweet.user}/>
            </div>
            <div className = 'btn btn-group px-0'>
        {(hideActions !== true && actionTweet) && 
            <React.Fragment>
                <ActionButton token = {token} tweet = {actionTweet} didPerformAction = {handlePerformAction} action = {{type: 'like', display: "Likes"}}/>
                <ActionButton token = {token} tweet = {actionTweet} didPerformAction = {handlePerformAction} action = {{type: 'unlike', display: "Unlike"}}/>
                <ActionButton token = {token} tweet = {actionTweet} didPerformAction = {handlePerformAction} action = {{type: 'retweet', display: 'Retweet'}}/>
            </React.Fragment>
        }
        {isDetail === true ? null : <button onClick = {handleLink} className = 'btn btn-primary btn-small'>View</button>}        
        </div>
        </div>
        </div>
  </div>
}