import React, {useState,useEffect, Component}  from 'react'
import {TweetsList} from './list'
import {TweetCreate} from './create'
import {apiTweetDetail} from './lookup'
import {Tweet} from './detail'
import {FeedList} from './feed'
import {connect} from 'react-redux';
import {logout} from "../actions/logout";

export function FeedComponent(props){
    const {username} = props
    const [newTweets, setNewTweets] = useState([])
    const cantweet  = props.cantweet === 'false' ? false : true

    const handleNewTweet = (newTweet) => {
        // backend api response handler
        let tempNewTweets = [...newTweets]
        tempNewTweets.unshift(newTweet)
        setNewTweets(tempNewTweets)
    }

    return <div className = {props.className}>
        {cantweet === true && <TweetCreate didTweet = {handleNewTweet} className = 'col-12 mb-3'/>}
        <FeedList newTweets = {newTweets} {...props}/>
    </div>
}

// export function TweetComponent(props){
//     const {user} = props
//     const [newTweets, setNewTweets] = useState([])
//     const cantweet  = props.cantweet === 'false' ? false : true

//     const handleNewTweet = (newTweet) => {
//         // backend api response handler
//         let tempNewTweets = [...newTweets]
//         tempNewTweets.unshift(newTweet)
//         setNewTweets(tempNewTweets)
//     }

//     return (
//         <div>
//             <h2>Tweet Me</h2>
//             <hr />
//             <div style={{textAlign: "right"}}>
//             {props.user.username} (<a onClick={props.logout}>logout</a>)
//             </div>
//             <p></p>
//             <p></p>
//             <div className = {props.className}>
//                 {cantweet === true && <TweetCreate username={props.user.username} didTweet = {handleNewTweet} className = 'col-12 mb-3'/>}
//                 <TweetsList newTweets = {newTweets} {...props}/>
//             </div>
//         </div>
//     )
// }

class TweetComponent extends Component{

    constructor(props) {
        super(props);
        this.handleNewTweet = this.handleNewTweet.bind(this);
    }

    handleNewTweet = (newTweet) => {
        // backend api response handler
        let tempNewTweets = [...this.state.tweets]
        tempNewTweets.unshift(newTweet)
        this.setState({tweets: tempNewTweets})
    }

    state = {
        tweets: []
    }
        
    render(){

        return(
            <div>
                <h2>Tweet Me</h2>
                <hr />
                <div style={{textAlign: "right"}}>
                {this.props.user.username} (<a onClick={this.props.logout}>logout</a>)
                </div>
                <p></p>
                <p></p>
                <div className = {this.props.className}>
                    {this.props.canTweet === true && <TweetCreate token = {this.props.token} username={this.props.user.username} didTweet = {this.handleNewTweet} className = 'col-12 mb-3'/>}
                    <TweetsList token = {this.props.token} newTweets = {this.state.tweets} {...this.props}/>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.auth.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(logout())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TweetComponent);

export function TweetDetailComponent(props){
    const {tweetId} = props
    const [didLookUp, setDidLookup] = useState(false)
    const [tweet, setTweet] = useState(null)

    const handleBackendLookup =  (response, status) => {
        if (status === 200){
            setTweet(response)
        }else{
            alert("Error occured")
        }
    }
    useEffect(() => {
        if (didLookUp === false){
            apiTweetDetail(tweetId, handleBackendLookup)
            setDidLookup(true)
        }

    }, [tweetId, didLookUp, setDidLookup])



    return tweet === null ? null : <Tweet tweet = {tweet} className = {props.className}/>
}
