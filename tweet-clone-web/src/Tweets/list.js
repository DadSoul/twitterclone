import React, {useState, useEffect}  from 'react'

import {
    apiTweetList
} from './lookup'

import {Tweet} from './detail'


export function TweetsList(props){
    const [tweetsInit, setTweetsInit] = useState([])
    const [tweets, setTweets]         = useState([])
    const [tweetsDidSet, setTweetsDidSet]         = useState(false)
    const [nextUrl, setNextUrl]       = useState(null)
    const [prevUrl, setPrevUrl]       = useState(null)
    const {token}                     = props

    useEffect(() => {
        let final = [...props.newTweets].concat(tweetsInit)
        if (final.length !== tweets.length){
            setTweets(final)
        }
    }, [props.newTweets, tweets, tweetsInit])
      
    useEffect(() => {
        if (tweetsDidSet === false){
            const handleTweetListLookUp = (response, status) => {
                if (status === 200){
                    setNextUrl(response.next);
                    setPrevUrl(response.prev);
                    setTweetsInit(response.results)
                    setTweetsDidSet(true)
                }else{
                    alert("There was an error")
                }
            }
            apiTweetList(props.username, handleTweetListLookUp) 
        }
    }, [tweetsInit, setTweetsDidSet, tweetsDidSet, props.username])

    const handleDidRetweet = (newTweet) => {
        const updateTweetsInit = [...tweetsInit]
        updateTweetsInit.unshift(newTweet)
        setTweetsInit(updateTweetsInit)
        const updateFinalTweets = [...tweets]
        updateFinalTweets.unshift(tweets)
        setTweets(updateFinalTweets)
    }

    const handleLoadNext = (event) => {
        event.preventDefault()
        if (nextUrl !== null){
            const handleLoadNextResponse = (response, status) => {
                if (status === 200){
                    setNextUrl(response.next);  
                    setPrevUrl(response.prev);
                    const newTweets = [...tweets].concat(response.results)
                    setTweetsInit(newTweets)
                    setTweets(newTweets)
                }else{
                    alert("There was an error")
                }
            }
            apiTweetList(props.username, handleLoadNextResponse, nextUrl)
        } 
    }

    return <React.Fragment>{tweets.map((item, index) => {
      return <Tweet
        tweet = {item} 
        didRetweet = {handleDidRetweet}
        className = 'my-5 py-5 border bg-white text-dark' key = {`${index} - ${item.id}`}
        key = {`${index}-${item.id}`}
        token = {token}
        />
    })}
    { nextUrl !== null && <button className = 'btn btn-outline btn-primary' onClick = {handleLoadNext}>LoadNext</button>}
    </React.Fragment>
}