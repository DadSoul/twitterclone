import React, {useState}   from 'react'
import {apiTweetCreate} from './lookup'

export function TweetCreate(props){
    const {username}    = props
    const textAreaRef   = React.createRef()
    const {didTweet}    = props
    const {token}       = props
    const [isRegistrationNeeded, setIsRegistrationNeeded] = useState(false)

    const handleBackendUpdate = (response, status) => {
        // backend api response handler
        if (status === 201){
            didTweet(response)
            setIsRegistrationNeeded(false)
        }else{
            setIsRegistrationNeeded(true)
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        const newValue = textAreaRef.current.value 
        // backend api request
        apiTweetCreate(newValue, handleBackendUpdate, token)
        textAreaRef.current.value = ""
    }

    return  <div className = {props.className}>
                <form onSubmit = {handleSubmit}>
                    <textarea ref = {textAreaRef} required = {true} className = 'form-control'></textarea>
                    <button type='submit' className = 'btn btn-primary my-3'>Tweet</button>
                </form>
            </div>
}




  
