import React, {useState, useEffect}  from 'react'

import {
    apiTweetFeed
} from './lookup'

import {Tweet} from './detail'


export function FeedList(props){

    const [tweetsInit, setTweetsInit] = useState([])
    const [tweets, setTweets]         = useState([])
    const [tweetsDidSet, setTweetsDidSet]         = useState(false)
    const [nextUrl, setNextUrl]       = useState(null)
    const [prevUrl, setPrevUrl]       = useState(null)

    useEffect(() => {
        let final = [...props.newTweets].concat(tweetsInit)
        if (final.length !== tweets.length){
            setTweets(final)
        }
    }, [props.newTweets, tweets, tweetsInit])
      
    useEffect(() => {
        if (tweetsDidSet === false){
            const handleTweetListLookUp = (response, status) => {
                if (status === 200){
                    setNextUrl(response.next);
                    setPrevUrl(response.prev);
                    setTweetsInit(response.results)
                    setTweetsDidSet(true)
                }
            }
            apiTweetFeed(handleTweetListLookUp) 
        }
    }, [tweetsInit, setTweetsDidSet, tweetsDidSet, props.username])

    const handleDidRetweet = (newTweet) => {
        const updateTweetsInit = [...tweetsInit]
        updateTweetsInit.unshift(newTweet)
        setTweetsInit(updateTweetsInit)
        const updateFinalTweets = [...tweets]
        updateFinalTweets.unshift(tweets)
        setTweets(updateFinalTweets)
    }

    const handleLoadNext = (event) => {
        event.preventDefault()
        if (nextUrl !== null){
            const handleLoadNextResponse = (response, status) => {
                if (status === 200){
                    setNextUrl(response.next);  
                    setPrevUrl(response.prev);
                    const newTweets = [...tweets].concat(response.results)
                    setTweetsInit(newTweets)
                    setTweets(newTweets)
                }
            }
            apiTweetFeed(handleLoadNextResponse)
        } 
    }

    return <React.Fragment>{tweets.map((item, index) => {
      return <Tweet
        tweet = {item} 
        didRetweet = {handleDidRetweet}
        className = 'my-5 py-5 border bg-white text-dark' key = {`${index} - ${item.id}`}
        key = {`${index}-${item.id}`}/>
    })}
    { nextUrl !== null && <button className = 'btn btn-outline btn-primary' onClick = {handleLoadNext}>LoadNext</button>}
    </React.Fragment>
}