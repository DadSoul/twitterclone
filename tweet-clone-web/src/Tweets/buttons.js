import React, {useState, useEffect}  from 'react'

import {
    apiTweetAction
} from './lookup'

export function ActionButton(props){
    const {tweet, action, didPerformAction}   = props
    const type              = action.type
    const className         = props.className ? props.className : 'btn btn-primary btn-small'
    const actionDisplay     = action.display ? action.display : 'Action'
    const likes = tweet.likes ? tweet.likes : 0 
    const {token}                             = props

    const handleActionBackendEvent = (response, status) =>{
        console.log(response, status)
        if ((status === 200 || status === 201) && didPerformAction){
            didPerformAction(response, status)
        }
    }
  
    const handleClick = (event) => {
        event.preventDefault ()
        apiTweetAction(tweet.id, type, handleActionBackendEvent, props.token)
    }

    const display            = type === 'like' ?  `${likes } ${actionDisplay}` : actionDisplay
    return <button className = {className}  onClick = {handleClick}>{display}</button>
  }