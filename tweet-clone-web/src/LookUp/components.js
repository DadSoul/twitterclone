

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
}

export function backendLookup(method, endPoint, callback, data, token = null){
    let jsonData;

    if (data){
        jsonData = JSON.stringify(data);
    }

    const xhr               = new XMLHttpRequest()
    const url               = `http://localhost:8000/api${endPoint}`
    const responseType      = "json" 
    xhr.responseType        = responseType
    const tokenLength       = document.cookie.length
    const csrfttokenLength  = "csrftoken".length
    var csrftoken           = document.cookie.substring(csrfttokenLength + 1)//document.cookie['csrftoken'] //getCookie('csrftoken');

    
    xhr.open(method, url)
    xhr.setRequestHeader("Content-Type", "application/json")
    
    if (token != null){
        console.log("Authorization")
        console.log(token)
        xhr.setRequestHeader("Authorization", `Token ${token}`)
    }

    if (csrftoken.length !== 0){
        console.log("header was set")
        xhr.setRequestHeader("HTTP_X_REQUESTED_WITH", 'XMLHttpRequest')
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')
        xhr.setRequestHeader('X-CSRFToken', csrftoken)
    }
    
    xhr.onload = function() {
        // if (xhr.status === 403){
        //     const detail = xhr.response.detail 
        //     if (detail === "Authentication credentials were not provided."){
        //         if (window.location.href.indexOf("login") === -1){
        //             window.location.href = '/login?showLoginRequired=true'
        //         }
        //         // window.location.href = '/login?showLoginRequired=true'
        //     }
        // }

        callback(xhr.response, xhr.status)
    }
    
    xhr.onerror = function(e) {
      console.log(e)
      callback({"message":"problem with request"}, 400)
    }

    xhr.send(jsonData)
}


