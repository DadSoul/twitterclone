export const logout = () => {
    return (dispatch, getState) => {
      let headers = {"Content-Type": "application/json"};
      const url               = `http://localhost:8000/api/logout/`
      return fetch(url, {headers, body: "", method: "POST"})
        .then(res => {
          if (res.status === 204) {
            return {status: res.status, data: {}};
          } else if (res.status < 500) {
            return res.json().then(data => {
              return {status: res.status, data};
            })
          } else {
            console.log("Server Error!");
            throw res;
          }
        })
        .then(res => {
          if (res.status === 204) {
            dispatch({type: 'LOGOUT_SUCCESSFUL'});
            return res.data;
          } else if (res.status === 403 || res.status === 401) {
            dispatch({type: "AUTHENTICATION_ERROR", data: res.data});
            throw res.data;
          }
        })
    }
  }