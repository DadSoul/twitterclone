import { combineReducers } from "redux";
import auth from './auth'

const tweetApp = combineReducers({
    auth,
})

export default tweetApp