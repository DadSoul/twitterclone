from rest_framework import authentication
from django.contrib.auth import get_user_model
from rest_framework.authentication import SessionAuthentication

User = get_user_model()

class DevAuthentication(authentication.BasicAuthentication):
    def authenticate(self, request):
        username = request.META.get('HTTP_X_USERNAME')
        print(username)
        qs      = User.objects.filter(id = 1)
        user    = qs.order_by("?").first() 
        return (user, None)